**Break Down Of Time Spent On The Challenge**

I spend around *three hours* on this project.

- Since majority of experience is with Flask, I had to spend around 30 mins or so reading Django documentation.
- Around two hours of my time was spent creating the ORM model, setting up URL and testing the data consumption.
- The rest of the time roughly 30 mins was spend setting up the API endpoint and testing it.

---

## Comments

**Django ORM**

I did not spend time on developing an upload file feature. Instead, the data is automatically consumed once the url *http://127.0.0.1:8000/* is opened.
I place the .csv file inside my project file. It is loaded as a Pandas dataframe. Then row by row of dataframe is consumed by the Django ORM.
The process is not as efficient as I would like it to be. However, I did not spend time developing an async process.

Please note that every time the above url is visited, my program clears the database table of any existing data before consuming the data in .csv file.

**API**

The API endpoint is */api/get_data/*

It will output all the entries inside the sqlite table.

The following curl command can be used to pull data using the API:

*curl -H 'Accept: application/json; indent=4' http://127.0.0.1:8000/api/get_data/*