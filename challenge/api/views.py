from django.shortcuts import render
from backend_excercise.models import ChallengeData
from rest_framework import viewsets
from api.serializers import ChallengeDataSerializer


class ChallengeDataViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = ChallengeData.objects.all()
    serializer_class = ChallengeDataSerializer