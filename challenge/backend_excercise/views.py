from django.shortcuts import render
from django.http import HttpResponse
from .models import ChallengeData
import pandas as pd

def upload_data(request):
    df = pd.read_csv(
        'challenge_data.csv',
        delimiter=','
    )

    df.replace({pd.np.nan: None}, inplace=True)

    #drop all existing Objects in model
    ChallengeData.objects.all().delete()

    for row in df.itertuples():
        data = ChallengeData.objects.create(
            area_unit=row.area_unit,
            bathrooms=row.bathrooms,
            bedrooms=row.bedrooms,
            home_size=row.home_size,
            home_type=row.home_type,
            last_sold_date=row.home_type,
            last_sold_price=row.last_sold_price,
            link=row.link,
            price=row.price,
            property_size=row.property_size,
            rent_price=row.rent_price,
            rentzestimate_last_updated=row.rentzestimate_last_updated,
            tax_value=row.tax_value,
            tax_year=row.tax_year,
            year_built=row.year_built,
            zestimate_amount=row.zestimate_amount,
            zestimate_last_updated=row.zestimate_last_updated,
            zillow_id=row.zillow_id,
            address=row.address,
            city=row.city,
            state=row.state,
            zipcode=row.zipcode
        )

    return HttpResponse('<h1>Finished Consuming Data</h1>')