from django.apps import AppConfig


class BackendExcerciseConfig(AppConfig):
    name = 'backend_excercise'
