from django.db import models

# Create your models here.

class ChallengeData(models.Model):
    area_unit = models.TextField(
        max_length=15, default=None, 
        blank=True, null=True
        )

    bathrooms = models.FloatField(
        default=None, blank=True, null=True
        )

    bedrooms = models.IntegerField(
        default=None, blank=True, null=True
        )

    home_size = models.IntegerField(
        default=None, blank=True, null=True
        )

    home_type = models.CharField(
        max_length=30, default=None, 
        blank=True, null=True
        )

    last_sold_date = models.CharField(
        max_length=10, default=None, 
        blank=True, null=True
        )

    last_sold_price = models.IntegerField(
        default=None, blank=True, null=True
        )

    link = models.URLField(
        max_length=128, default=None, 
        blank=True, null=True
        )

    price = models.CharField(
        max_length=30, default=None, 
        blank=True, null=True
        )

    property_size = models.IntegerField(
        default=None, blank=True, null=True
        )

    rent_price = models.CharField(
        max_length=30, default=None, 
        blank=True, null=True
        )

    rentzestimate_last_updated = models.CharField(
        max_length=10, default=None, 
        blank=True, null=True
        )

    tax_value = models.IntegerField(
        default=None, blank=True, null=True
        )

    tax_year = models.IntegerField(
        default=None, blank=True, null=True
        )

    year_built = models.IntegerField(
        default=None, blank=True, null=True
        )

    zestimate_amount = models.IntegerField(
        default=None, blank=True, null=True
        )

    zestimate_last_updated = models.CharField(
        max_length=10, default=None, 
        blank=True, null=True
        )

    zillow_id = models.IntegerField(unique=True)

    address = models.CharField(
        max_length=128, default=None, 
        blank=True, null=True
        )

    city = models.TextField(
        max_length=30, default=None, 
        blank=True, null=True
        )
        
    state = models.TextField(
        max_length=2, default=None, 
        blank=True, null=True
        )

    zipcode = models.IntegerField(
        default=None, blank=True, null=True
        )

